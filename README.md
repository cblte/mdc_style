# README #

### MDC Style ###

This repo stores the master copy of the MDC style files in CSL (citation style format). The McMaster Divinity College style is used by the [college students and faculty](https://mcmasterdivinity.ca/programs/resources-forms) and for several related publications such as the [McMaster Journal of Theology and Ministry](http://www.mcmaster.ca/mjtm/), and [Biblical and Ancient Greek Linguistics](http://bagl.org/).

There are three different style files: the master file (named "MDC-style.csl"), and the annotated bibliography style ("MDC-annotated-bibliography"). 
The latter includes annotations created by the user through the citation manager software, whereas the master file does not include annotations. For example, 
>Barr, James. The Semantics of Biblical Language. London: Oxford University Press, 1961
>
>This is an annotation, entered in the “Extra” field in Zotero. If anyone uses the annotated style with another citation manager, please post on the discussion thread regarding which field should be used for annotations in which managers.

The third file, "Endnote_MDC-Style.ens" is an Endnote-compatible version of the MDC style. However, this file is not being actively maintained.

Finally, a Microsoft Word template with preset MDC-style conforming styles and title page has also been included.

### How do I get set up? ###

1. Download the relevant source files (either "MDC-style.csl" or "MDC-annotated-bibliography.csl", or both) to your computer. 
2. Open the files one at a time with your bibliography management software (e.g. Zotero, Endnote, Mendeley, Papers, etc.). (Please use a search engine for help with this step, as each software management tool is different. In general, you can simply right click the style file once it is downloaded to your computer and click "open with" and select your bibliographic manager from the list.)
3. When generating a bibliography using your software, select the relevant style.

### Contribution guidelines ###

Please submit any queries on errors or updates to the CBLTE discourse forum [discussion thread](http://forum.cblte.org/t/mdc-style-files-for-citation-managers/105?u=ryder).

### Who do I talk to? ###

Questions should be directed to the CBLTE discussion thread.

### Disclaimer ###

These resources are freely provided by the CBLTE, and the CBLTE absolves itself of any responsibility for the correctness of the output. It is always the author's ultimate responsibility to ensure the correctness and accuracy of the final form of citations.